import React, {Component} from 'react';
import ProductViewer from '../../common/productViewer/';
const data = [{
    id: 'iphone',
    title: 'iPhone',
    previewImage: '/images/iphone.png',
    showcaseImage: 'https://www.aussar.es/31547-large_default/iphone-x-64gb-silver.jpg',
    text: 'this is a iPhone'
},{
    id: 'macbook',
    title: 'Macbook',
    previewImage: '/images/macbook.png',
    showcaseImage: 'https://www.aussar.es/31547-large_default/iphone-x-64gb-silver.jpg',
    text: 'this is a macbook'
},{
    id: 'imac',
    title: 'iMac',
    previewImage: '/images/imac.png',
    showcaseImage: 'https://www.aussar.es/31547-large_default/iphone-x-64gb-silver.jpg',
    text: 'this is an imac'
}];

export default class HomePage extends Component {
    render() {
        return (
            <div>
                <div className='textWrapper'>
                    <h1>HOMEPAGE</h1>
                    <ProductViewer
                        data={data}
                    />
                </div>
            </div>
        );
    }
}