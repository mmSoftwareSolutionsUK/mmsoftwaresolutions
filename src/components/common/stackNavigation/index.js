import React, {Component} from 'react';
import _ from 'lodash';
import './stackNavigation.css';
import {Squeeze} from 'react-burgers';

export default class StackNavigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showNavBar: false,
            animateBurger: false,
            selectedPage: {},
            currentPage: {},
            enlargeOtherPage: null,
            animatePageEnlarge: false,
            transitionDuration: false,
            animatePageToFullScreen: null,
            disableBurgerClick: false
        };
        this.renderNavigationOptions = this.renderNavigationOptions.bind(this);
    }

    componentDidMount() {
        const {pages} = this.props;
        if (pages && Array.isArray(pages) && pages.length > 0) {
            this.setState({currentPage: pages[0]})
        }
    }

    renderNavigationOptions() {
        const {pages} = this.props;
        if (pages && Array.isArray(pages) && pages.length > 0) {
            return pages.map((page, i) => {
                return (
                    <div
                        className='navigationTitleButton'
                        key={i}
                        onClick={() => this.state.currentPage.id !== page.id && this.changePage(page)}
                    >
                        <div className='navigationTextButtonWrapper'>
                            <h2 className={`navigationTitleText ${page.id === this.state.currentPage.id && 'navigationTitleTextCurrentPage'}`}>
                                {page.title}
                            </h2>
                            {this.state.currentPage.id !== page.id &&
                            <div className='animateUnderline'>
                            </div>
                            }
                        </div>
                    </div>
                );
            });
        }
    }


    changePage(page) {
        let sequence = (page.id - this.state.currentPage.id);
        if (sequence < 0) {
            if (sequence === -1) {
                sequence = 2;
            } else {
                sequence = page.id + 1;
            }
        }
        if (this.state.currentPage.id === this.props.pages.length - 2) {
            if (page.id === 0) {
                sequence = 2;
            }
        }
        this.setState({
            selectedPage: {
                ...page,
                sequence
            },
            animatePageToFullScreen: sequence >= 3 ? 2 : sequence,
            animateBurger: !this.state.animateBurger,
            disableBurgerClick: true
        });
        setTimeout(() => this.setState({
            enlargeOtherPage: null,
            currentPage: page,
            selectedPage: null,
            animatePageToFullScreen: null,
            disableBurgerClick: false,
            animateBurger: false,
            showNavBar: false,
            transitionDuration: true
        }), 500);
    }

    showHideNavBar() {
        this.setState({
            showNavBar: !this.state.showNavBar,
            animateBurger: !this.state.animateBurger,
            transitionDuration: false
        });
    }

    renderFullPage() {
        const {currentPage, showNavBar} = this.state;
        if (!_.isEmpty(currentPage)) {
            return (
                <div
                    onClick={() => showNavBar && this.showHideNavBar()}
                    className={`page ${showNavBar ? 'pageStackMode pageStackMode-0' : ''}`}
                    style={{
                        transitionDuration: !this.state.enlargeOtherPage && !this.state.transitionDuration ? '0.5s' : '0s'
                    }}
                >
                    {currentPage.component}
                </div>
            );
        }

    }

    renderPages() {
        const {currentPage, animatePageToFullScreen} = this.state;
        if (this.state.showNavBar) {
            const {id} = currentPage;
            const {pages} = this.props;
            const numberOfPages = pages.length - 1;
            const pagesLeft = numberOfPages - id;
            const nextPageId = pagesLeft === 0 ? 0 : id + 1;
            const pageAfterNextId = pagesLeft === 1 ? 0 : nextPageId + 1;

            return (
                <React.Fragment>
                    <div
                        onClick={() => {
                            this.changePage(pages[nextPageId]);
                        }}
                        className={`page ${animatePageToFullScreen === 1 ? '' : 'pageStackMode pageStackMode-1'}`}
                    >
                        {pages[nextPageId].component}
                    </div>
                    <div
                        onClick={() => {
                            this.changePage(pages[pageAfterNextId]);
                        }}
                        className={`page ${animatePageToFullScreen === 2 ? '' : 'pageStackMode pageStackMode-2'}`}
                    >
                        {animatePageToFullScreen === 2 && this.state.selectedPage.component}
                    </div>
                </React.Fragment>
            );
        }
    }


    render() {
        console.log('animatePageToFullScreen', this.state.animatePageToFullScreen);
        return (
            <div className='mainBody'>
                <div className='burgerButton' onClick={() => !this.state.disableBurgerClick && this.showHideNavBar()}>
                    <Squeeze
                        color={this.state.animateBurger ? '#545a64' : 'white'}
                        width={30}
                        lineHeight={2}
                        lineSpacing={8}
                        borderRadius={100}
                        active={this.state.animateBurger}
                    />
                </div>
                {this.state.showNavBar &&
                <div className='navigationBar'>
                    {this.renderNavigationOptions()}
                </div>
                }

                {this.renderFullPage()}

                {this.state.showNavBar && this.renderPages()}
            </div>
        )
    }

}