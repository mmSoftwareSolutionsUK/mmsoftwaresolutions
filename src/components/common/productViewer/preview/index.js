import React, {Component} from 'react';
import Arrow from '../arrow/';
import '../productViewer.css';

export default class PreviewMode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentImageID: 0,
            nextImageID: 0,
            onArrowClick: ''
        }
    }

    getCSSClass(hideNextPage = false) {
        const {onArrowClick} = this.state;
        if (onArrowClick === 'right') {
            return `translateLeft ${hideNextPage && 'hideRight'}`;
        }
        if (onArrowClick === 'left') {
            return `translateRight ${hideNextPage && 'hideLeft'}`;
        }
        return '';
    }

    onClickArrow(direction) {
        this.setState({onArrowClick: direction});
        const {currentImageID} = this.state;
        const {data} = this.props;
        const lengthOfData = data.length - 1;
        const directionValue = (direction === 'right' ? 1 : (direction === 'left' ? -1 : 0));
        const expectedNextImageID = directionValue + currentImageID;

        console.log('-----------------------------');
        console.log('lengthOfData',lengthOfData);
        console.log('directionValue',directionValue);
        console.log('currentImageID',currentImageID);
        console.log('expectedNextImageID',expectedNextImageID);
        console.log('expectedNextImageID > lengthOfData',expectedNextImageID > lengthOfData);
        console.log('expectedNextImageID < lengthOfData',expectedNextImageID < lengthOfData);
        let actualNextImageID;
        if (expectedNextImageID > lengthOfData) {
            actualNextImageID = 0;
            this.setState({nextImageID: actualNextImageID})
        } else if (expectedNextImageID < 0) {
            actualNextImageID = lengthOfData;
            this.setState({nextImageID: actualNextImageID})
        } else {
            actualNextImageID = expectedNextImageID;
            this.setState({nextImageID: actualNextImageID})
        }

        console.log('actualNextImageID',actualNextImageID);
        setTimeout(() => this.setState({onArrowClick: '', currentImageID: actualNextImageID}), 1000)
    }

    renderPreviewImages() {
        return (
            <div
                className={`previewImageWrapper ${this.getCSSClass()}`}>
                <img
                    className='previewImage'
                    src={this.props.data[this.state.currentImageID].previewImage}
                />
            </div>
        );
    }

    renderNextImage() {
        if (this.state.onArrowClick !== '') {
            return (
                <div
                    className={`previewNextImageWrapper ${this.getCSSClass(true)}`}>
                    <img
                        className='previewImage'
                        src={this.props.data[this.state.nextImageID].previewImage}
                    />
                </div>
            );
        }
    }

    render() {
        return (
            <div style={{
                backgroundColor: 'transparent',
                display: 'inline-block',
                position: 'relative'
            }}>
                <div style={{width: 500}}>
                    <Arrow direction='left' onClick={this.onClickArrow.bind(this)}/>
                    <div
                        style={{
                            overflow: 'hidden'
                        }}
                    >
                        {this.renderPreviewImages()}
                        {this.renderNextImage()}
                    </div>
                    <Arrow direction='right' onClick={this.onClickArrow.bind(this)}/>
                </div>
                {/*<h1 className={`productTitle ${this.state.onArrowClick}`}>*/}
                {/*{this.props.data[this.state.currentImage].title}*/}
                {/*</h1>*/}
            </div>
        );
    }
}