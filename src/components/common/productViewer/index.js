import React, {Component} from 'react';
import PreviewMode from "./preview";

export default class ProductViewer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div
                style={{
                    position: 'absolute',
                    transform: 'translate(-50%,-50%)',
                    left: '50vw',
                    top: '50vh'
                }}
            >
                <PreviewMode
                    data={this.props.data}
                />
            </div>
        );
    }
}