import React from 'react';

const Arrow = ({direction,onClick}) => (
    <div
        style={{
            [direction]: 0,
            transform: 'translateY(-50%)',
            position: 'absolute',
            top: '50%'
        }}
        onClick={()=>onClick(direction)}
    >
        <img
            src={`/images/${direction}Chevron.png`}
            style={{
                height: 40,
                width: 40,
            }}
        />
    </div>
);

export default Arrow;