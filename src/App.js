import React, { Component } from 'react';
import StackNavigation from "./components/common/stackNavigation/";
import ProductViewer from "./components/common/productViewer/";
import HomePage from './components/pages/homePage/';
import AboutPage from './components/pages/aboutPage/';
import ContactPage from './components/pages/contactPage/';
import CuntPage from './components/pages/cuntPage/';
import MattIsACuntPage from './components/pages/mattIsACuntPage/';

const stackNavigationPages = [{
    id: 0,
    title: "HOME PAGE",
    component: <HomePage/>
},{
    id: 1,
    title: "ABOUT PAGE",
    component: <AboutPage/>
},{
    id: 2,
    title: "CONTACT PAGE",
    component: <ContactPage/>
},{
    id: 3,
    title: "CUNT PAGE",
    component: <CuntPage/>
},{
    id: 4,
    title: "MATT IS A CUNT PAGE",
    component: <MattIsACuntPage/>
}];

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <StackNavigation
            pages={stackNavigationPages}
        />
      </React.Fragment>
    );
  }
}

export default App;
